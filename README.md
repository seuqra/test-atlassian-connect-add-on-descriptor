# Simple code to test Connect add-on for Confluence

This repository is associated to:

* question [Confluence ondemand: Cannot display macro body of a dynamicContentMacros](https://answers.atlassian.com/questions/27330364)
* bug https://ecosystem.atlassian.net/browse/AC-1730